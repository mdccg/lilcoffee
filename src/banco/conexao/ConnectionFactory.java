package banco.conexao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;

public class ConnectionFactory {
    public static final String PORT = "5432";
    public static final String BD = "bd";
    public static final String USER = "matheus";
    public static final String KEYWORD = "mdccg";

    public static Connection con() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://localhost:" + PORT + "/" + BD, USER, KEYWORD);
    }
}
