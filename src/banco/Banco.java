package banco;

import spaghetti.PythonIO.print;
import spaghetti.PythonIO.input;

import banco.conexao.ConnectionFactory;

import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Banco {

    public static void cacar(Connection con) throws SQLException {
        String nome = input("nome do estudante a ser caçado: ");
        nome += "%";

        PreparedStatement ps = con.prepareStatement("select * from estudantes where nome like ?;");
        ps.setString(1, nome);

        ResultSet rs = ps.executeQuery();

        String res = new String();
        while(rs.next()) {
            res += "cpf: " + rs.getString("cpf") + "\n" +
                    "nome: " + rs.getString("nome") + "\n" +
                    "idade: " + rs.getInt("idade") + "\n" +
                    "~~~~~~~~~~~~~~~~~~~~\n";
        }

        if(res.length() == 0)
            res = "nenhum estudante foi encontrado ='((((";

        print(res);
    }

    public static void listar(Connection con) throws SQLException {
        PreparedStatement ps = con.prepareStatement("select * from estudantes;");
        ResultSet rs = ps.executeQuery();

        String res = new String();
        while(rs.next()) {
            res += "cpf: " + rs.getString("cpf") + "\n" +
                    "nome: " + rs.getString("nome") + "\n" +
                    "idade: " + rs.getInt("idade") + "\n" +
                    "~~~~~~~~~~~~~~~~~~~~\n";
        }

        if(res.length() == 0)
            res = "nenhum estudante foi encontrado ='((((";

        print(res);
    }

    public static void deletar(Connection con) throws SQLException {
        String cpf = input("cpf do estudante a ser deletado: ");
        cpf += "%";

        PreparedStatement ps = con.prepareStatement("delete from estudantes where cpf like ?;");
        ps.setString(1, cpf);
        ps.execute();
        ps.close();
    }

    public static void cadastrar(Connection con) throws SQLException {
        String cpf = input("cpf do estudante: ");

        if(cpf.length() == 0)
            cpf = "undefined";

        String nome = input("nome do estudante: ");

        if(nome.length() == 0)
            nome = "undefined";

        Integer idade;
        try {
            idade = Integer.parseInt(input("idade do estudante: "));

        } catch(NumberFormatException e) { idade = 404; }

        String sql = "insert into estudantes (cpf, nome, idade) values (?, ?, ?);";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, cpf);
        ps.setString(2, nome);
        ps.setInt(3, idade);
        ps.execute();
        ps.close();

        print("estudante cadastrado com êxito!\n\n" +
                "cpf: " + cpf + "\n" +
                "nome: " + nome + "\n" +
                "idade: " + idade + "\n");
    }

    public static void main(String[] args) throws SQLException {
        Connection con = ConnectionFactory.con();

        while(true) {
            String chr = input("\tsisteminha acadêmico do if feito com muito carinho sz\n\n" +
                    "(c)adastrar estudante\n" +
                    "(l)istar todos os estudantes\n" +
                    "ca(ç)ar estudante\n" +
                    "(d)eletar estudante\n" +
                    "(p)ower off");

            switch(chr.toLowerCase()) {
                case "c":
                    cadastrar(con);
                    break;
                case "l":
                    listar(con);
                    break;
                case "ç":
                    cacar(con);
                    break;
                case "d":
                    deletar(con);
                    break;
                case "p":
                    con.close();
                    return;
                default:
                    print("opção inválida, sherlock holmes");
            }
        }
    }
}
