/**
 * @author mdccg
 * @since 2018
 *
 * .___  ___.      ___      .______      ___          _______     _______.___________. _______  __          ___      .______
 * |   \/   |     /   \     |   _  \    /   \        |   ____|   /       |           ||   ____||  |        /   \     |   _  \
 * |  \  /  |    /  ^  \    |  |_)  |  /  ^  \       |  |__     |   (----`---|  |----`|  |__   |  |       /  ^  \    |  |_)  |
 * |  |\/|  |   /  /_\  \   |   ___/  /  /_\  \      |   __|     \   \       |  |     |   __|  |  |      /  /_\  \   |      /
 * |  |  |  |  /  _____  \  |  |     /  _____  \     |  |____.----)   |      |  |     |  |____ |  `----./  _____  \  |  |\  \----.
 * |__|  |__| /__/     \__\ | _|    /__/     \__\    |_______|_______/       |__|     |_______||_______/__/     \__\ | _| `._____|
 *
 * INSTRUÇÕES:
 *
 * 1. Planetas (vértices) no arquivo "MapaEstelar.txt", seguidos de "EOP" para encerrar a leitura;
 *
 * 2. Grafos, seguindo a estrutura:
 * (vértice) (aresta) (vértice) (valor)
 *
 * Ex.:
 * Coruscant --> Yavin_4 10
 * Yavin_4 <-- Coruscant 10
 * Coruscant <--> Yavin_4 10
 *
 * Direcionados os grafos, "EOG" para encerrar a leitura;
 *
 * 3. Perguntas, contendo APENAS dois vértices;
 *
 * Ex.:
 * Coruscant Yavin_4
 *
 * Saída:
 * A distância entre Coruscant e Yavin_4 é de 10 parsec(s)!
 *
 * Feitas as perguntas, "EOF" para encerrar a leitura.
 */

package sw;

import java.util.Scanner;
import java.util.HashMap;
import java.io.FileReader;
import java.io.FileNotFoundException;

public class MapaEstelar {
    public static int INF = 1 << 29;

    public static int insercao(Scanner sc, HashMap negri) {
        int C = 0;
        for(String str = sc.nextLine(); !str.equals("EOP"); str = sc.nextLine())
            negri.put(str, C++);

        return C;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String src = "src/sw/MapaEstelar.txt";
        Scanner sc = new Scanner(new FileReader(src));

        HashMap<String, Integer> negri = new HashMap<>();

        while(sc.hasNext()) {
            // leitura de vértices
            int C = insercao(sc, negri);

            int dist[][] = new int[C][C];

            for(int i = 0; i < C; ++i)
                for(int j = 0; j < C; ++j)
                    dist[i][j] = INF;

            for(String str = sc.nextLine(); !str.equals("EOG"); str = sc.nextLine()) {
                String[] strtok = str.split(" ");

                int a = negri.get(strtok[0]); // Coruscant

                String or = strtok[1]; // -->
                /** @see or indica se o grafo é orientado ou não */

                /*
                 * por ex.:
                 *
                 * orientado
                 * A --> B 10
                 *
                 * não-orientado
                 * A <--> B 10
                 */

                int b = negri.get(strtok[2]); // Dagobah
                int c = Integer.parseInt(strtok[3]); // 10 parsecs

                // grafo direcionado!
                if(or.equals("-->"))
                    dist[a][b] = c;
                else if(or.equals("<--"))
                    dist[b][a] = c;
                else if(or.equals("<-->")) {
                    dist[a][b] = c;
                    dist[b][a] = c;
                }
            }

            for (int k = 0; k < C; ++k)
                for (int i = 0; i < C; ++i)
                    for (int j = 0; j < C; ++j)
                        if (dist[i][j] > dist[i][k] + dist[k][j])
                            dist[i][j] = dist[i][k] + dist[k][j];

            /*for(int i = 0; i < C; ++i) {
                for(int j = 0; j < C; ++j)
                    System.out.print(dist[i][j] + " ");

                System.out.println();
            }*/

            boolean cons = false;
            /** @see cons considera que o piloto estelar pode dar uma voltinha (partida = destino) */

            for(String str = sc.nextLine(); !str.equals("EOF"); str = sc.nextLine()) {
                String[] strtok = str.split(" ");

                String pla1 = strtok[0];
                String pla2 = strtok[1];

                int a;
                try {
                    a = negri.get(pla1);
                } catch(NullPointerException e) {
                    System.out.println(pla1 + " não existe!"); continue;

                }

                int b;
                try {
                    b = negri.get(pla2);

                } catch(NullPointerException e) {
                    System.out.println(pla2 + " não existe!"); continue;

                }

                if(!cons && pla1.equals(pla2)) { System.out.println("Você já está em " + pla1 + "!"); continue; }

                if(dist[a][b] == INF)
                    System.out.println("Há um campo de asteróides entre " + pla1 + " e " + pla2 + "!");
                else
                    System.out.printf("A distância entre %s e %s é de %d parsec(s)!\n", pla1, pla2, dist[a][b]);
            }
        }
    }
}
