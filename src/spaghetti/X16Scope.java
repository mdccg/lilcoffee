package spaghetti;

import java.math.BigDecimal;
import java.math.BigInteger;

public class X16Scope {
    public static void main(String[] args) {
        double d1 = 0.1, d2 = 0.2;

        System.out.println("IEEE 754 --> " + (d1 + d2));

        BigDecimal b1 = BigDecimal.valueOf(0.1), b2 = BigDecimal.valueOf(0.2);

        System.out.println("x16 Scope --> " + b1.add(b2));

        BigInteger b3 = new BigInteger("2147483648"), b4 = BigInteger.valueOf(1 << 30);

        System.out.println("2 ** 31 - 2 ** 30 = " + (b3.subtract(b4)).toString());
        System.out.println("(2 ^ 30) * 2 = " + (b4.multiply(BigInteger.valueOf(2))).toString());
    }
}