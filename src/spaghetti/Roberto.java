package spaghetti;

class Aniversariante {
  private String nome;

  public Aniversariante(String nome) {
    this.nome = nome;
  }

  public Aniversariante() {}

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
}

public class Roberto {
  public static void main(String[] args) {
    Aniversariante aniversariante = new Aniversariante();
    aniversariante.setNome("Roberto");

    String felizAniversario = String.format("Parabéns, %s! Muita saúde, "
      + "muita paz, prosperidade e muitos anos de vida.", aniversariante.getNome());
    
    System.out.println(felizAniversario);
  }
}