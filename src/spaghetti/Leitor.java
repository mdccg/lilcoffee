package spaghetti;

import java.util.Scanner;
import java.io.FileReader;
import java.io.FileNotFoundException;

public class Leitor {
    public static void main(String[] args) throws FileNotFoundException {
        String src = "src/spaghetti/Leitor.txt";
        Scanner sc = new Scanner(new FileReader(src));

        while(sc.hasNext()) {
            System.out.println("Tirei do arquivo externo: " + sc.nextLine());
        }
    }
}
