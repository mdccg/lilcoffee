package spaghetti;

import java.util.Scanner;
import java.math.BigInteger;
import static spaghetti.PythonIO.print;
import static spaghetti.PythonIO.input;

public class Excecao {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        BigInteger N = null;

        do {
            try {
                N = new BigInteger(input("Insira um número inteiro qualquer:\n" +
                        "Por ex.: 1, 3, 2147483648, -5 ou etc."));

            } catch(NumberFormatException e) {
                print("Oh, que pena! Não deu certo.\n" +
                        "Mas você não pode desistir ainda!\n" +
                        "Como dizia Charlies Chaplin,\n" +
                        "'A persistência é o caminho do êxito.'");

            }

        }while(N == null);

        print("Parabéns, você conseguiu!\n" +
                "Até a próxima, javaneiro! ;)");
    }
}