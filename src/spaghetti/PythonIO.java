package spaghetti;

import javax.swing.JOptionPane;

public class PythonIO {

    public static String input() { return JOptionPane.showInputDialog(null, ""); }

    public static String input(String str) { return JOptionPane.showInputDialog(null, str); }

    public static void print(String str) { JOptionPane.showMessageDialog(null, str); }

    public static void main(String[] args) {
        System.out.println("JOptionPane eh chato");

        print("Olá, Aquidas!");

        String str = input("Qual é o seu nome?");

        print("É um prazer te conhecer, " + str + "!");
    }
}